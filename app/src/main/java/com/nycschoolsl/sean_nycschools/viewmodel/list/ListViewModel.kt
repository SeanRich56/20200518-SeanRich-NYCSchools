package com.nycschoolsl.sean_nycschools.viewmodel.list

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.VisibleForTesting
import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel
import com.nycschoolsl.sean_nycschools.network.ApiClient
import com.nycschoolsl.sean_nycschools.util.RxSingleSchedulers
import com.nycschoolsl.sean_nycschools.viewmodel.BaseViewModel
import com.nycschoolsl.sean_nycschools.viewmodel.list.adapters.ListAdapter
import io.reactivex.disposables.CompositeDisposable
import java.util.*

class ListViewModel(var context: Context) : BaseViewModel() {

    private var disposable: CompositeDisposable? = CompositeDisposable()
    val data: ArrayList<NYCSchoolDataModel> = ArrayList()
    //RX Schedulers for multi threading
    private var rxSingleSchedulers: RxSingleSchedulers = RxSingleSchedulers.DEFAULT
    private var apiService: ApiClient = ApiClient()
    val adapter: ListAdapter = ListAdapter(context, data)

    @VisibleForTesting
    constructor(
        apiClient: ApiClient,
        rxSingleSchedulers: RxSingleSchedulers,
        context: Context
    ) : this(context) {
        this.apiService = apiClient
        this.rxSingleSchedulers = rxSingleSchedulers
    }

    @SuppressLint("CheckResult")
    fun loadData() {
        onLoading()
        disposable?.add(
            apiService.getList()
                .compose(rxSingleSchedulers.applySchedulers())
                .subscribe(this::onSuccess, this::onError)
        )
    }

    private fun onSuccess(models: List<NYCSchoolDataModel>) {
        data.addAll(models)
        adapter.notifyDataSetChanged()
        dataLoading.value = false
    }

    private fun onError(error: Throwable) {
        errorMessage = error.message ?: ""
        dataLoading.value = false
    }

    private fun onLoading() {
        dataLoading.value = true
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.run {
            clear()
            null
        }
    }
}
