package com.nycschoolsl.sean_nycschools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


open class BaseViewModel : ViewModel() {
    val dataLoading = MutableLiveData(false)
    var errorMessage: String = ""
}