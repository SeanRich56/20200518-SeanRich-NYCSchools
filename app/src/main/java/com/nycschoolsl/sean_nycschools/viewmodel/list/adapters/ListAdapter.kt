package com.nycschoolsl.sean_nycschools.viewmodel.list.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nycschoolsl.sean_nycschools.R
import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel
import com.nycschoolsl.sean_nycschools.util.CustomRecyclerViewAdapter
import com.nycschoolsl.sean_nycschools.util.Utils.dateFormatYYYYMMDD
import com.nycschoolsl.sean_nycschools.util.inflate
import kotlinx.android.synthetic.main.fragment_item_detail.view.*
import kotlinx.android.synthetic.main.item_school_list.view.*
import kotlinx.android.synthetic.main.item_school_list.view.con_layoutMain
import java.util.*
import kotlinx.android.synthetic.main.item_school_list.view.tv_website as tv_website1
import kotlinx.android.synthetic.main.item_school_list.view.tv_phone_number as tv_phone_number1
import kotlinx.android.synthetic.main.item_school_list.view.tv_school_name as tv_school_name1

class ListAdapter(
    private val mContext: Context,
    private val _List: ArrayList<NYCSchoolDataModel>
) : CustomRecyclerViewAdapter<NYCSchoolDataModel, ListAdapter.MyViewHolder>() {

    private var selectedPosition = -1
    var listener: IListAdapter? = null

    /**
     * method to update the list.
     * also @see com.nycschoolsl.sean_nycschools.util.CustomBindingAdapters#bind()
     */

    override fun updateData(data: List<NYCSchoolDataModel>) {
        if (data.isEmpty()) {
            this._List.clear()
        } else {
            this._List.addAll(data)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder =
        MyViewHolder(parent.inflate(R.layout.item_school_list))

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) = holder.loadData(position)

    override fun getItemCount(): Int = _List.size


    inner class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun loadData(position: Int) {
            val model = _List[position]
            view.apply {
                setBackgroundColor(getBackgroundColor(position))

                con_layoutMain.tag = position
                con_layoutMain.setOnClickListener {
                    listener?.itemSelected(model)
                    selectedPosition = position
                    notifyDataSetChanged()
                }

                tv_school_name.text = model.school_name
                tv_phone_number.text = model.phone_number
                tv_school_email.text = model.school_email

                tv_website.text=model.website

            }

        }

        private fun getBackgroundColor(position: Int): Int =
            if (selectedPosition == position)
                ContextCompat.getColor(mContext, R.color.colorAccent)
            else ContextCompat.getColor(mContext, android.R.color.white)

    }

    interface IListAdapter {
        fun itemSelected(model: NYCSchoolDataModel)
    }
}