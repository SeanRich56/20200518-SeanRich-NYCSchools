package com.nycschoolsl.sean_nycschools.viewmodel.details

import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel
import com.nycschoolsl.sean_nycschools.viewmodel.BaseViewModel

class DetailsViewModel : BaseViewModel() {

    var selectedData: NYCSchoolDataModel? = null

}