package com.nycschoolsl.sean_nycschools.view.activity

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.nycschoolsl.sean_nycschools.R
import com.nycschoolsl.sean_nycschools.databinding.ActivityItemListBinding
import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel
import com.nycschoolsl.sean_nycschools.util.openActivity
import com.nycschoolsl.sean_nycschools.util.replaceFragment
import com.nycschoolsl.sean_nycschools.util.showLoadingDialog
import com.nycschoolsl.sean_nycschools.util.viewModel
import com.nycschoolsl.sean_nycschools.view.fragment.ItemDetailFragment
import com.nycschoolsl.sean_nycschools.view.fragment.ItemDetailFragment.Companion.ARG_ITEM_ID
import com.nycschoolsl.sean_nycschools.viewmodel.list.ListViewModel
import com.nycschoolsl.sean_nycschools.viewmodel.list.adapters.ListAdapter


/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [ItemDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class ItemListActivity : AppCompatActivity(), ListAdapter.IListAdapter {

    private lateinit var itemListBinding: ActivityItemListBinding
    private val listModel: ListViewModel by viewModel {
        ListViewModel(this)
    }
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var mTwoPane: Boolean = false
    private val dialog: AlertDialog by showLoadingDialog {
        cancelable = false
        setMessage("Loading data...")
        setRetryVisibility()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemListBinding = DataBindingUtil.setContentView(this, R.layout.activity_item_list)
        listModel.adapter.listener = this
        listModel.loadData()
        itemListBinding.viewModel = listModel
        itemListBinding.lifecycleOwner = this
        val toolbar = itemListBinding.toolbar
        setSupportActionBar(toolbar)
        toolbar.title = title
        listModel.dataLoading.observe(this, Observer { aBoolean ->
            if (aBoolean!!) {
                dialog.show()
            } else
                dialog.dismiss()
        })

        if (itemListBinding.itemListContainer.itemDetailContainer != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true
        }

        itemListBinding.itemListContainer.itemList.addItemDecoration(
            DividerItemDecoration(
                itemListBinding.itemListContainer.itemList.context,
                DividerItemDecoration.VERTICAL
            )
        )

    }

    /**
     * Replace the fragment based on if its a tab view or a mobile view
     */
    override fun itemSelected(model: NYCSchoolDataModel) {
        when {
            mTwoPane ->
                replaceFragment(R.id.item_detail_container, ItemDetailFragment()) {
                    putParcelable(ARG_ITEM_ID, model)
                }
            !mTwoPane ->
                openActivity(ItemDetailActivity::class.java) {
                    putParcelable(ARG_ITEM_ID, model)
                }
        }
    }
}