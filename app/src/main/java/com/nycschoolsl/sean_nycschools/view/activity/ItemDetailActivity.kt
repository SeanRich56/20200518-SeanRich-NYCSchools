package com.nycschoolsl.sean_nycschools.view.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.nycschoolsl.sean_nycschools.R
import com.nycschoolsl.sean_nycschools.databinding.ActivityItemDetailBinding
import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel
import com.nycschoolsl.sean_nycschools.util.addFragment
import com.nycschoolsl.sean_nycschools.util.viewModel
import com.nycschoolsl.sean_nycschools.view.fragment.ItemDetailFragment
import com.nycschoolsl.sean_nycschools.view.fragment.ItemDetailFragment.Companion.ARG_ITEM_ID
import com.nycschoolsl.sean_nycschools.viewmodel.details.DetailsViewModel

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [ItemListActivity].
 */
class ItemDetailActivity : AppCompatActivity() {

    private lateinit var activityItemDetailBinding: ActivityItemDetailBinding
    private val viewModel: DetailsViewModel by viewModel {
        DetailsViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityItemDetailBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_item_detail)
        activityItemDetailBinding.apply {
            viewModel = viewModel
            lifecycleOwner = this@ItemDetailActivity
            setSupportActionBar(detailToolbar)
        }

        val dataModel = intent.getParcelableExtra<NYCSchoolDataModel>(ARG_ITEM_ID)
        viewModel.selectedData = dataModel

        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            addFragment(R.id.item_detail_container, ItemDetailFragment()) {
                putParcelable(ARG_ITEM_ID, dataModel)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {

            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
