package com.nycschoolsl.sean_nycschools.network

import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel

import io.reactivex.Single
import retrofit2.http.GET

interface RestService {
    @GET(Constants.URL_DATA)
    fun getListData(): Single<List<NYCSchoolDataModel>>
}