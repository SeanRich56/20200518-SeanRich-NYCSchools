package com.nycschoolsl.sean_nycschools.network


import com.nycschoolsl.sean_nycschools.model.NYCSchoolDataModel
import io.reactivex.Single

class ApiClient {

    private val apiService: RestService =
        ApiEndPoint.retrofitInstance.create(RestService::class.java)

    fun getList(): Single<List<NYCSchoolDataModel>> =
        apiService.getListData()
}