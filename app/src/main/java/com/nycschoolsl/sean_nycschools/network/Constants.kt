package com.nycschoolsl.sean_nycschools.network

object Constants {
    const val BASE_URL = "https://data.cityofnewyork.us/"
    const val URL_DATA = "resource/s3k6-pzi2.json"
}